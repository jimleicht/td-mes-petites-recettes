Quel fichier modifié pour changer l'url local du site ?
>Le fichier .env, la variable PROJECT_BASE_URL

Quelle est l'url de phpmyadmin ?
>localhost:8085

Quel fichier contient les paramètres de la database ?
>docker-compose.yml sous la mention environment: de l'image mariadb
>Quelques exemples : MYSQL_ROOT_PASSWORD, MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD

Comment sont-ils lu par drupal ?
>Ils sont lus lors du lancement de docker compose up. Les variables d'environnements sont envoyés dans les conteneurs pour ainsi permettre la connexion à 
>la base de données. Cette connexion se fait depuis le fichier sites/default/settings.php de l'appli Drupal


Quelle est la commande drush qui permet de se connecter en temps qu'administrateur ?
>drush uli, génére un lien de connexion direct unique
>Ce lien expire ensuite pour des raisons de sécurité

Binôme :
Janelle GENONCEAU et Jimmy LEICHTMANN